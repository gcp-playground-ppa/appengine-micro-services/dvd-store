package fr.ppaquin.microservices.dvdstore.service;

import fr.ppaquin.microservices.storage.dao.DvdRepository;
import fr.ppaquin.microservices.dvdstore.dto.Dvd;
import fr.ppaquin.microservices.storage.entity.DvdEntity;
import fr.ppaquin.microservices.dvdstore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.dvdstore.exception.EanNotFoundException;
import fr.ppaquin.microservices.dvdstore.mapper.DvdEntityMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DvdStoreService {

    @Autowired
    private DvdRepository dvdRepository;
    @Autowired
    private DvdEntityMapper dvdEntityMapper;

    public Dvd save(Dvd dvd, String userName, boolean update) throws EanAlreadyDefinedException, EanNotFoundException {
        if (update) {
            return updateDvdEntity(dvd, userName);
        } else {
            return createDvdEntity(dvd, userName);
        }
    }

    private Dvd createDvdEntity(Dvd dvd, String userName) throws EanAlreadyDefinedException {
        Optional<DvdEntity> optionalEntity = dvdRepository.findByEanAndOwner(dvd.getEan(), userName);
        DvdEntity entity;
        if (optionalEntity.isPresent()) {
            throw new EanAlreadyDefinedException("Ean exist in DB : " + dvd.getEan());
        } else {
            entity = createEntityFromDto(dvd, userName);
        }
        return dvdEntityMapper.map(dvdRepository.save(entity));
    }

    private Dvd updateDvdEntity(Dvd dvd, String userName) throws EanNotFoundException {
        Optional<DvdEntity> optionalEntity = dvdRepository.findByEanAndOwner(dvd.getEan(), userName);
        DvdEntity entity;
        if (optionalEntity.isPresent()) {
            entity = optionalEntity.get();
            updateEntityWithDto(dvd, entity);
        } else {
            throw new EanNotFoundException("Ean does not exist in DB : " + dvd.getEan());
        }
        return dvdEntityMapper.map(dvdRepository.save(entity));
    }

    private void updateEntityWithDto(Dvd dvd, DvdEntity entity) {
        String title = dvd.getTitle() == null ? "" : dvd.getTitle();
        String cover = dvd.getCover() == null ? "" : dvd.getCover();
        entity.setCover(cover);
        entity.setTitle(title);
        if (dvd.getDvdfr() > 0) {
            entity.setDvdfr(dvd.getDvdfr());
        } else {
            entity.setDvdfr(null);
        }
        if (dvd.getYear() > 0) {
            entity.setYear(dvd.getYear());
        } else {
            entity.setYear(null);
        }
    }

    private DvdEntity createEntityFromDto(Dvd dvd, String userName) {
        String title = dvd.getTitle() == null ? "" : dvd.getTitle();
        String cover = dvd.getCover() == null ? "" : dvd.getCover();
        DvdEntity entity = new DvdEntity(dvd.getEan(), title, cover, userName);
        if (dvd.getDvdfr() > 0) {
            entity.setDvdfr(dvd.getDvdfr());
        }
        if (dvd.getYear() > 0) {
            entity.setYear(dvd.getYear());
        }
        return entity;
    }

    public List<Dvd> list(String userName) {
        return dvdRepository.findByOwner(userName)
                .stream()
                .map(dvdEntityMapper::map)
                .collect(Collectors.toList());
    }

    public int delete(Dvd dvd, String userName) throws EanNotFoundException {
        Optional<DvdEntity> optionalEntity = dvdRepository.findByEanAndOwner(dvd.getEan(), userName);
        if (optionalEntity.isPresent()) {
            dvdRepository.delete(optionalEntity.get());
        } else {
            throw new EanNotFoundException("Ean does not exist in DB : " + dvd.getEan());
        }
        return 1;
    }

}
