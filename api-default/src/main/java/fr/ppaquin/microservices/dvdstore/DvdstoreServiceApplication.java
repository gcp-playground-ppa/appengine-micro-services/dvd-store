package fr.ppaquin.microservices.dvdstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.data.firestore.repository.config.EnableReactiveFirestoreRepositories;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"fr.ppaquin.microservices.storage", "fr.ppaquin.microservices.dvdstore"})
public class DvdstoreServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvdstoreServiceApplication.class, args);
	}

}
