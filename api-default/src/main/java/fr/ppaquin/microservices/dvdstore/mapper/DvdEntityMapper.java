package fr.ppaquin.microservices.dvdstore.mapper;

import org.springframework.stereotype.Component;

import fr.ppaquin.microservices.dvdstore.dto.Dvd;
import fr.ppaquin.microservices.storage.entity.DvdEntity;

@Component
public class DvdEntityMapper {

	public Dvd map(DvdEntity entity) {
		int dvdfr = entity.getDvdfr() != null ? entity.getDvdfr() : 0;
		int year = entity.getYear() != null ? entity.getYear() : 0;
		return new Dvd(dvdfr, entity.getEan(), entity.getTitle(), year, entity.getCover());
	}
}
