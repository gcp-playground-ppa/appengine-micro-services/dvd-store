package fr.ppaquin.microservices.dvdstore;

import fr.ppaquin.microservices.dvdstore.dto.Dvd;
import fr.ppaquin.microservices.dvdstore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.dvdstore.exception.EanNotFoundException;
import fr.ppaquin.microservices.dvdstore.service.DvdStoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/")
public class DvdStoreEndPointController {

    @Autowired
    DvdStoreService dvdStoreService;

    @PutMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public List<Dvd> createDvd(@RequestBody final Dvd dvd, Principal user)
            throws EanAlreadyDefinedException, EanNotFoundException {
        dvdStoreService.save(dvd, user.getName(), false);
        return dvdStoreService.list(user.getName());
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.OK)
    public List<Dvd> updateDvd(@RequestBody final Dvd dvd, Principal user)
            throws EanAlreadyDefinedException, EanNotFoundException {
        dvdStoreService.save(dvd, user.getName(), true);
        return dvdStoreService.list(user.getName());
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.OK)
    public List<Dvd> deleteDvd(@RequestBody final Dvd dvd, Principal user) throws EanNotFoundException {
		dvdStoreService.delete(dvd, user.getName());
        return dvdStoreService.list(user.getName());
    }

    @GetMapping
    public List<Dvd> listDvds(Principal user) {
		return dvdStoreService.list(user.getName());
    }

    @ExceptionHandler({EanAlreadyDefinedException.class})
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public String handleEanAlreadyExistException(EanAlreadyDefinedException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler({EanNotFoundException.class})
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public String handleEanNotFoundException(EanNotFoundException exception) {
        return exception.getMessage();
    }

}
