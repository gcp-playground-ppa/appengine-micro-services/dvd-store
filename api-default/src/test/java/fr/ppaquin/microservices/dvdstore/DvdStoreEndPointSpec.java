package fr.ppaquin.microservices.dvdstore;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.ahunigel.test.security.oauth2.MockTokenServices;
import fr.ppaquin.microservices.dvdstore.dto.Dvd;
import fr.ppaquin.microservices.dvdstore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.dvdstore.exception.EanNotFoundException;
import fr.ppaquin.microservices.dvdstore.service.DvdStoreService;
import fr.ppaquin.microservices.dvdstore.utils.AbstractEndPoint;
import fr.ppaquin.microservices.dvdstore.utils.WithOAuthSubject;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@WebAppConfiguration
@SpringBootTest(classes = DvdstoreServiceApplication.class)
@MockTokenServices
@AutoConfigureMockMvc
public class DvdStoreEndPointSpec extends AbstractEndPoint {

    @MockBean
    DvdStoreService dvdStoreService;

    Dvd dummyDvd1 = new Dvd("123");
    Dvd dummyDvd2 = new Dvd("456");

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void createApiShouldCreateDvdAndReturnFullUsersDvdList() throws Exception {
        givenDvdStoreServiceReturning(dummyDvd1);

        whenRequest(put("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        thenCreateDvd1AndExpect(new Dvd[]{dummyDvd1, dummyDvd2});
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void createApiShouldReturn409WhenDvdAlreadyExist() throws Exception {
        givenExistingEanOnDvd1();

        whenRequest(put("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        assertEquals(HttpStatus.CONFLICT.value(), mvcResult.getResponse().getStatus());
        verify(dvdStoreService, times(1)).save(new Dvd("123"), "username", false);
        thenNoMoreInteractions();
    }

    @Test
    void createApiShouldReturn401WhenPutAsAnonymous() throws Exception {
        whenRequest(put("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
        thenNoMoreInteractions();
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void updateApiShouldUpdateDvdAndReturnFullUsersDvdList() throws Exception {
        givenDvdStoreServiceReturning(dummyDvd1);

        whenRequest(post("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        thenUpdateDvd1AndExpect(new Dvd[]{dummyDvd1, dummyDvd2});
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void updateApiShouldReturn204WhenUpdatingEanThatDoesntExist() throws Exception {
        givenEanNotFoundOnSaveDvd1();

        whenRequest(post("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        verify(dvdStoreService, times(1)).save(new Dvd("123"), "username", true);
        assertEquals(HttpStatus.NO_CONTENT.value(), mvcResult.getResponse().getStatus());
        thenNoMoreInteractions();
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void getDvdShouldListFullUsersDvdList() throws Exception {
        givenUsersDvdList(Arrays.asList(dummyDvd1, dummyDvd2));

        whenRequest(get("/"));

        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        verify(dvdStoreService, times(1)).list("username");
        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(new Dvd[]{dummyDvd1, dummyDvd2}).isEqualTo(mapper.readValue(receivedJson, Dvd[].class));

        thenNoMoreInteractions();
    }

    @Test
    void getDvdShouldReturn401WhenGetAsAnonymous() throws Exception {
        whenRequest(get("/"));

        assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
        thenNoMoreInteractions();
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void deleteApiShouldDeleteDvdAndReturnFullUsersDvdList() throws Exception {
        givenDvd1AsDeletedDvd();

        whenRequest(delete("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        thenDeleteDvd1AndExpect(new Dvd[]{dummyDvd2});
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void deleteApiShouldReturn204WhenDeletingEanThatDoesntExist() throws Exception {
        givenEanNotFoundOnDeleteDvd1();

        whenRequest(delete("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        verify(dvdStoreService, times(1)).delete(new Dvd("123"), "username");
        assertEquals(HttpStatus.NO_CONTENT.value(), mvcResult.getResponse().getStatus());
        thenNoMoreInteractions();
    }

    @Test
    void deleteApiShouldReturn401WhenDeleteAsAnonymous() throws Exception {
        whenRequest(delete("/").content(toJson(dummyDvd1)).contentType(APPLICATION_JSON_VALUE));

        assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
        thenNoMoreInteractions();
    }

    private void givenDvdStoreServiceReturning(Dvd dvd) throws Exception {
        when(dvdStoreService.save(any(Dvd.class), anyString(), anyBoolean())).thenReturn(dvd);
        givenUsersDvdList(Arrays.asList(dummyDvd1, dummyDvd2));
    }

    private void givenDvd1AsDeletedDvd() throws Exception {
        when(dvdStoreService.delete(dummyDvd1, "username")).thenReturn(1);
        givenUsersDvdList(Arrays.asList(dummyDvd2));
    }

    private void givenExistingEanOnDvd1() throws Exception {
        when(dvdStoreService.save(dummyDvd1, "username", false))
                .thenThrow(new EanAlreadyDefinedException("EAN 123 already exist"));
    }

    private void givenEanNotFoundOnSaveDvd1() throws Exception {
        when(dvdStoreService.save(dummyDvd1, "username", true))
                .thenThrow(new EanNotFoundException("EAN 123 does not exist exist"));
    }

    private void givenEanNotFoundOnDeleteDvd1() throws Exception {
        when(dvdStoreService.delete(dummyDvd1, "username"))
                .thenThrow(new EanNotFoundException("EAN 123 does not exist exist"));
    }

    private void givenUsersDvdList(List<Dvd> dvds) {
        when(dvdStoreService.list("username")).thenReturn(dvds);
    }

    private void thenCreateDvd1AndExpect(Dvd[] dvds) throws Exception {
        assertEquals(HttpStatus.CREATED.value(), mvcResult.getResponse().getStatus());
        verify(dvdStoreService, times(1)).save(new Dvd("123"), "username", false);
        verify(dvdStoreService, times(1)).list("username");

        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(dvds).isEqualTo(mapper.readValue(receivedJson, Dvd[].class));

        thenNoMoreInteractions();
    }

    private void thenUpdateDvd1AndExpect(Dvd[] dvds) throws UnsupportedEncodingException, JsonProcessingException,
            EanAlreadyDefinedException, EanNotFoundException {
        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        verify(dvdStoreService, times(1)).save(new Dvd("123"), "username", true);
        verify(dvdStoreService, times(1)).list("username");

        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(dvds).isEqualTo(mapper.readValue(receivedJson, Dvd[].class));

        thenNoMoreInteractions();
    }

    private void thenDeleteDvd1AndExpect(Dvd[] dvds) throws UnsupportedEncodingException, JsonProcessingException, EanNotFoundException {
        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        verify(dvdStoreService, times(1)).delete(new Dvd("123"), "username");
        verify(dvdStoreService, times(1)).list("username");

        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(dvds).isEqualTo(mapper.readValue(receivedJson, Dvd[].class));

        thenNoMoreInteractions();
    }

    private void thenNoMoreInteractions() {
        verifyNoMoreInteractions(dvdStoreService);
    }


}
