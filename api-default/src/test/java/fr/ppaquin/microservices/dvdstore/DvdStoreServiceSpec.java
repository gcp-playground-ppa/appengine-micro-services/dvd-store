package fr.ppaquin.microservices.dvdstore;

import fr.ppaquin.microservices.dvdstore.dto.Dvd;
import fr.ppaquin.microservices.dvdstore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.dvdstore.exception.EanNotFoundException;
import fr.ppaquin.microservices.dvdstore.service.DvdStoreService;
import fr.ppaquin.microservices.storage.dao.DvdRepository;
import fr.ppaquin.microservices.storage.entity.DvdEntity;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = DvdstoreServiceApplication.class)
public class DvdStoreServiceSpec {

    @Autowired
    DvdStoreService dvdStoreService;

    @MockBean
    DvdRepository dvdRepository;

    @Test
    void shouldListUserDvds() throws Exception {

        DvdEntity entity1 = new DvdEntity(1L, "123", "", "", "username", null, null);
        DvdEntity entity2 = new DvdEntity(2L, "456", "", "", "username", null, null);

        when(dvdRepository.findByOwner("username")).thenReturn(Arrays.asList(entity1, entity2));

        List<Dvd> dtos = dvdStoreService.list("username");

        verify(dvdRepository, times(1)).findByOwner("username");
        assertThat(dtos).isEqualTo(Arrays.asList(new Dvd("123"), new Dvd("456")));
        thenNoMoreInteractions();

    }

    @Test
    void shouldCreatedDvd() throws Exception {

        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());
        when(dvdRepository.save(any(DvdEntity.class))).thenReturn(
                new DvdEntity(1L, "123", "", "", "username", null, null));

        dvdStoreService.save(new Dvd("123"), "username", false);

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        verify(dvdRepository, times(1)).save(new DvdEntity("123", "", "", "username"));
        thenNoMoreInteractions();

    }

    @Test
    void shouldThrowExceptionWhenCreateWithExistingEan() throws Exception {

        DvdEntity entity = new DvdEntity(1L, "123", "", "", "username", null, null);
        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(entity));
        try {
            dvdStoreService.save(new Dvd("123"), "username", false);
            fail("Exception should be thrown here");
        } catch (EanAlreadyDefinedException e) {
            // Expected Exception on existing EAN.
        }

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    @Test
    void shouldUpdateExistingDvd() throws Exception {

        DvdEntity entity = new DvdEntity(1L, "123", "", "", "username", null, null);
        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(entity));
        when(dvdRepository.save(any(DvdEntity.class))).thenReturn(entity);

        dvdStoreService.save(new Dvd(0, "123", "New Title", 0, ""), "username", true);

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        verify(dvdRepository, times(1)).save(new DvdEntity(1L, "123", "New Title", "", "username", null, null));
        thenNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionWhenUpdatingMissingEan() throws Exception {

        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());

        try {
            dvdStoreService.save(new Dvd("123"), "username", true);
            fail("Exception should be thrown here");
        } catch (EanNotFoundException e) {
            // Expected Exception on existing EAN.
        }

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    @Test
    void shouldDeleteExistingDvd() throws Exception {

        DvdEntity entity = new DvdEntity(1L, "123", "", "", "username", null, null);
        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(entity));

        int deleted = dvdStoreService.delete(new Dvd(0, "123", "", 0, ""), "username");

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        verify(dvdRepository, times(1)).delete(new DvdEntity(1L, "123", "", "", "username", null, null));
        assertThat(deleted).isEqualTo(1);
        thenNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionWhenDeletingMissingEan() throws Exception {

        when(dvdRepository.findByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());

        try {
            dvdStoreService.delete(new Dvd("123"), "username");
            fail("Exception should be thrown here");
        } catch (EanNotFoundException e) {
            // Expected Exception on existing EAN.
        }

        verify(dvdRepository, times(1)).findByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    private void thenNoMoreInteractions() {
        verifyNoMoreInteractions(dvdRepository);
    }

}
