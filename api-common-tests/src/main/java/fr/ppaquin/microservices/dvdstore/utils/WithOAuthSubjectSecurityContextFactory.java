package fr.ppaquin.microservices.dvdstore.utils;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import com.google.common.collect.ImmutableMap;

public class WithOAuthSubjectSecurityContextFactory implements WithSecurityContextFactory<WithOAuthSubject> {

	private DefaultAccessTokenConverter defaultAccessTokenConverter = new DefaultAccessTokenConverter();

	@Autowired
	ConfigurableListableBeanFactory factory;

	@Override
	public SecurityContext createSecurityContext(WithOAuthSubject withOAuthSubject) {
		SecurityContext context = SecurityContextHolder.createEmptyContext();

		List<String> scopes = Arrays.asList(withOAuthSubject.scopes()).stream()
				.map(property -> factory.resolveEmbeddedValue(property)).collect(Collectors.toList());

		// Copy of response from
		// https://myidentityserver.com/identity/connect/accesstokenvalidation
		// @formatter:off
		Map<String, ?> remoteToken = ImmutableMap.<String, Object>builder() 
				.put("iss", "https://myfakeidentity.example.com/identity")
				.put("aud", "oauth2-resource")
				.put("exp", OffsetDateTime.now().plusDays(1L).toEpochSecond() + "")
				.put("nbf", OffsetDateTime.now().plusDays(1L).toEpochSecond() + "")
				.put("client_id", "my-client-id")
				.put("user_name", "username").put("scope", scopes)
				.put("sub", withOAuthSubject.subjectId())
				.put("auth_time", OffsetDateTime.now().toEpochSecond() + "")
				.put("idp", "idsrv").put("amr", "password")
				.put("authorities", scopes).build();
		// @formatter:on

		OAuth2Authentication authentication = defaultAccessTokenConverter.extractAuthentication(remoteToken);
		context.setAuthentication(authentication);
		return context;
	}
}
