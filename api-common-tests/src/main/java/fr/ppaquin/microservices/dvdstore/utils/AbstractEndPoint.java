package fr.ppaquin.microservices.dvdstore.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public abstract class AbstractEndPoint {

    protected MockMvc mockMvc;
    protected MvcResult mvcResult;

    protected ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void setup() {
        this.mvcResult = null;
    }

    protected void whenRequest(MockHttpServletRequestBuilder request) throws Exception {
        request.accept(APPLICATION_JSON_VALUE);
        mvcResult = this.mockMvc.perform(request).andReturn();
    }

    protected String toJson(Object jsonObject) throws JsonProcessingException {
        return mapper.writeValueAsString(jsonObject);
    }
}
