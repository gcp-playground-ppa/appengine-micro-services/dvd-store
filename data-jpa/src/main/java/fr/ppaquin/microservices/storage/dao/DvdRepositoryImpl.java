package fr.ppaquin.microservices.storage.dao;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import fr.ppaquin.microservices.storage.entity.DvdEntity;
import fr.ppaquin.microservices.storage.entity.QDvdEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public class DvdRepositoryImpl implements DvdRepositoryCustom {

    private static final QDvdEntity qDvd = QDvdEntity.dvdEntity;

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DvdEntity> findByOwner(final String ownerLogin) {
        return new JPAQuery<>(em).select(qDvd)
                .from(qDvd)
                .where(qDvd.username.eq(ownerLogin))
                .fetch();
    }

    @Override
    public Optional<DvdEntity> findByEanAndOwner(String ean, String ownerLogin) {
        final Predicate predicate = qDvd.ean.eq(ean).and(qDvd.username.eq(ownerLogin));

        return Optional.ofNullable(new JPAQuery<>(em).select(qDvd)
                                           .from(qDvd)
                                           .where(predicate)
                                           .fetchOne());
    }
}
