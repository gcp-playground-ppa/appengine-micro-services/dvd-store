package fr.ppaquin.microservices.storage.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import fr.ppaquin.microservices.storage.entity.DvdEntity;

public interface DvdRepository extends JpaRepository<DvdEntity, Long>, QuerydslPredicateExecutor<DvdEntity>, DvdRepositoryCustom {
}
