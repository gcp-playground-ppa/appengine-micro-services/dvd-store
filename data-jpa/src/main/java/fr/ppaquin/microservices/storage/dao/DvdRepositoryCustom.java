package fr.ppaquin.microservices.storage.dao;

import fr.ppaquin.microservices.storage.entity.DvdEntity;

import java.util.List;
import java.util.Optional;

public interface DvdRepositoryCustom {

    List<DvdEntity> findByOwner(final String ownerLogin);

    Optional<DvdEntity> findByEanAndOwner(final String ean, final String ownerLogin);
}
