package fr.ppaquin.microservices.storage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Table(name = "dvd", indexes = { @Index(name = "indx_username", columnList = "username", unique = false) })
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class DvdEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false)
	private Long id;

	@NonNull
	@Column(name = "ean", nullable = false, unique = false)
	private String ean;

	@NonNull
	@Column(name = "title", nullable = false)
	private String title;

	@NonNull
	@Column(name = "cover", nullable = false)
	private String cover;

	@NonNull
	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "dvdfr", nullable = true)
	private Integer dvdfr;

	@Column(name = "year", nullable = true)
	private Integer year;
}
