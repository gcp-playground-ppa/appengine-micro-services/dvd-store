package fr.ppaquin.microservices.fichestore.exception;

public class EanAlreadyDefinedException extends Exception {

    private static final long serialVersionUID = -7848974134361133125L;

    public EanAlreadyDefinedException(String message) {
        super(message);
    }

}
