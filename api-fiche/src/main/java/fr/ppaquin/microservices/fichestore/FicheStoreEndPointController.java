package fr.ppaquin.microservices.fichestore;

import fr.ppaquin.microservices.fichestore.dto.FicheDvd;
import fr.ppaquin.microservices.fichestore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.fichestore.exception.EanNotFoundException;
import fr.ppaquin.microservices.fichestore.service.FicheStoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.security.Principal;

@RestController
@RequestMapping("/fiche")
public class FicheStoreEndPointController {

    @Autowired
    private FicheStoreService ficheStoreService;

    @PutMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    public FicheDvd createFiche(@RequestBody final FicheDvd ficheDvd,
                                Principal user) throws EanAlreadyDefinedException, EanNotFoundException {
        return ficheStoreService.save(ficheDvd, user.getName(), false);
    }

    @PostMapping()
    @ResponseStatus(code = HttpStatus.OK)
    public FicheDvd updateFiche(@RequestBody final FicheDvd ficheDvd,
                                Principal user) throws EanAlreadyDefinedException, EanNotFoundException {
        return ficheStoreService.save(ficheDvd, user.getName(), true);
    }

    @GetMapping("/{ean}")
    @ResponseStatus(code = HttpStatus.OK)
    public FicheDvd getFiche(@PathVariable("ean") @NotBlank final String ean,
                             Principal user) throws EanNotFoundException {
        return ficheStoreService.findUsersFiche(ean, user.getName());
    }

    @ExceptionHandler({EanAlreadyDefinedException.class})
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public String handleEanAlreadyExistException(EanAlreadyDefinedException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler({EanNotFoundException.class})
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public String handleEanNotFoundException(EanNotFoundException exception) {
        return exception.getMessage();
    }
}
