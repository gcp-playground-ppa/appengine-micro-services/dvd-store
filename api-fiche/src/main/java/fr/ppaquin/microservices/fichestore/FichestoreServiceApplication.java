package fr.ppaquin.microservices.fichestore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"fr.ppaquin.microservices.storage", "fr.ppaquin.microservices.fichestore"})
public class FichestoreServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FichestoreServiceApplication.class, args);
    }
}
