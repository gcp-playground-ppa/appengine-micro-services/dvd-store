package fr.ppaquin.microservices.fichestore.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class FicheDvd {
    private int tmdbId;
    private String ean = "";
    private String title = "";
    private int year;
    private String cover = "";
    private String backdrop = "";
    private String overview = "";
    private List<String> genres = new ArrayList<>();

    public FicheDvd(String ean) {
        this.ean = ean;
    }
}
