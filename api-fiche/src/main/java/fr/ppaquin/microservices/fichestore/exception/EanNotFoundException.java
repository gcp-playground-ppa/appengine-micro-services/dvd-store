package fr.ppaquin.microservices.fichestore.exception;

public class EanNotFoundException extends Exception {

    private static final long serialVersionUID = 7828513414186775105L;

    public EanNotFoundException(String message) {
        super(message);
    }

    public EanNotFoundException(){
        super();
    }

}
