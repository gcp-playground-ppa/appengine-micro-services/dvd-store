package fr.ppaquin.microservices.fichestore.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class OAuth2ResourceServer extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {//@formatter:off
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers("/actuator/health").permitAll()
		.antMatchers("/v2/api-docs", "/webjars/**", "/swagger-resources/**", "/swagger-ui.html").permitAll()
		.antMatchers("/").hasAuthority("SCOPE_write:dvd")
		.and()
		.authorizeRequests()
		.anyRequest().authenticated()
		.and()
		.oauth2ResourceServer().jwt();
	}
	//@formatter:on
}
