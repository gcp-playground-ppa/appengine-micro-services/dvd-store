package fr.ppaquin.microservices.fichestore.service;

import fr.ppaquin.microservices.storage.dao.FicheRepository;
import fr.ppaquin.microservices.storage.entity.FicheEntity;
import fr.ppaquin.microservices.fichestore.dto.FicheDvd;
import fr.ppaquin.microservices.fichestore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.fichestore.exception.EanNotFoundException;
import fr.ppaquin.microservices.fichestore.mapper.FicheEntityMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@Service
public class FicheStoreService {

    @Autowired
    private FicheRepository ficheRepository;

    @Autowired
    private FicheEntityMapper ficheEntityMapper;

    public FicheDvd save(final FicheDvd ficheDvd, final String userName, final boolean update) throws EanAlreadyDefinedException, EanNotFoundException {
        if (update) {
            return updateFicheEntity(ficheDvd, userName);
        } else {
            return createFicheEntity(ficheDvd, userName);
        }
    }

    private FicheDvd createFicheEntity(final FicheDvd ficheDvd, final String userName) throws EanAlreadyDefinedException {
        final Optional<FicheEntity> optionalEntity = ficheRepository.findOneByEanAndOwner(ficheDvd.getEan(), userName);
        final FicheEntity entity;
        if (optionalEntity.isPresent()) {
            throw new EanAlreadyDefinedException("Fiche with given Ean exist in DB : " + ficheDvd.getEan());
        } else {
            entity = createEntityFromDto(ficheDvd, userName);
        }
        return ficheEntityMapper.map(ficheRepository.save(entity));
    }

    private FicheEntity createEntityFromDto(final FicheDvd ficheDvd, final String userName) {
        final FicheEntity entity = new FicheEntity();
        entity.setUsername(userName);
        this.updateEntityWithDto(ficheDvd, entity);
        return entity;
    }

    private FicheDvd updateFicheEntity(final FicheDvd ficheDvd, final String userName) throws EanNotFoundException {
        final Optional<FicheEntity> optionalEntity = ficheRepository.findOneByEanAndOwner(ficheDvd.getEan(), userName);
        final FicheEntity entity;
        if (optionalEntity.isPresent()) {
            entity = optionalEntity.get();
            this.updateEntityWithDto(ficheDvd, entity);
        } else {
            throw new EanNotFoundException("Fiche with given Ean not exist in DB : " + ficheDvd.getEan());
        }
        return ficheEntityMapper.map(ficheRepository.save(entity));
    }

    private void updateEntityWithDto(final FicheDvd ficheDvd, final FicheEntity entity) {
        entity.setEan(Objects.toString(ficheDvd.getEan(), ""));
        entity.setBackdrop(Objects.toString(ficheDvd.getBackdrop(), ""));
        entity.setCover(Objects.toString(ficheDvd.getCover(), ""));
        entity.setOverview(Objects.toString(ficheDvd.getOverview(), ""));
        entity.setTitle(Objects.toString(ficheDvd.getTitle(), ""));
        entity.setYear(ficheDvd.getYear());
        entity.setTmdbId(ficheDvd.getTmdbId());
        entity.setGenres(new ArrayList<>(ficheDvd.getGenres()));
    }

    public FicheDvd findUsersFiche(final String ean, final String username) throws EanNotFoundException {
        return ficheRepository.findOneByEanAndOwner(ean, username).map(ficheEntityMapper::map).orElseThrow(EanNotFoundException::new);
    }
}
