package fr.ppaquin.microservices.fichestore.mapper;

import fr.ppaquin.microservices.storage.entity.FicheEntity;
import fr.ppaquin.microservices.fichestore.dto.FicheDvd;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class FicheEntityMapper {

    public FicheDvd map(FicheEntity entity) {
        return new FicheDvd(entity.getTmdbId(),
                            entity.getEan(),
                            entity.getTitle(),
                            entity.getYear(),
                            entity.getCover(),
                            entity.getBackdrop(),
                            entity.getOverview(),
                            new ArrayList<>(entity.getGenres()));
    }
}
