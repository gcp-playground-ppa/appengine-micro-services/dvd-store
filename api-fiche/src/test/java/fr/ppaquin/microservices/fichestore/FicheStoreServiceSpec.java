package fr.ppaquin.microservices.fichestore;

import fr.ppaquin.microservices.storage.dao.FicheRepository;
import fr.ppaquin.microservices.storage.entity.FicheEntity;
import fr.ppaquin.microservices.fichestore.dto.FicheDvd;
import fr.ppaquin.microservices.fichestore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.fichestore.exception.EanNotFoundException;
import fr.ppaquin.microservices.fichestore.service.FicheStoreService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = FichestoreServiceApplication.class)
public class FicheStoreServiceSpec {

    //TODO test with JPA Repository

    @MockBean
    FicheRepository ficheRepository;

    @Autowired
    private FicheStoreService ficheStoreService;

    @Test
    void shouldFindUsersFiches() throws Exception {
        // Given
        FicheEntity entity = new FicheEntity();
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(entity));

        // When
        FicheDvd fiche = ficheStoreService.findUsersFiche("123", "username");

        // Then
        verify(ficheRepository).findOneByEanAndOwner("123", "username");
        assertThat(fiche).isEqualTo(new FicheDvd());
        thenNoMoreInteractions();
    }

    @Test
    void shouldThrowEanNotFoundExceptionWhenNoFicheFound() throws Exception {
        // Given
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());

        //When (+ expect exception)
        Assertions.assertThrows(EanNotFoundException.class, () -> {
            ficheStoreService.findUsersFiche("123", "username");
        });

        //Then
        verify(ficheRepository).findOneByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    @Test
    void shouldCreatedFiche() throws Exception {
        // Given
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());
        when(ficheRepository.save(any(FicheEntity.class))).thenReturn(new FicheEntity());

        //When
        ficheStoreService.save(new FicheDvd("123"), "username", false);

        // Then
        verify(ficheRepository, times(1)).findOneByEanAndOwner("123", "username");
        verify(ficheRepository, times(1)).save(newFicheEntity("123"));
        thenNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionWhenCreateWithExistingEan() throws Exception {
        // Given
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(newFicheEntity("123")));

        //When (+ expect exception)
        Assertions.assertThrows(EanAlreadyDefinedException.class, () -> {
            ficheStoreService.save(new FicheDvd("123"), "username", false);
        });

        //Then
        verify(ficheRepository, times(1)).findOneByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    @Test
    void shouldUpdateExistingDvd() throws Exception {
        // Given
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.of(newFicheEntity("123", "oldTitle")));
        when(ficheRepository.save(any(FicheEntity.class))).thenReturn(newFicheEntity("123", "newTitle"));

        // When
        FicheDvd updatedValue = ficheStoreService.save(new FicheDvd("123"), "username", true);

        // Then
        verify(ficheRepository, times(1)).findOneByEanAndOwner("123", "username");
        verify(ficheRepository, times(1)).save(newFicheEntity("123"));
        assertThat(updatedValue).hasFieldOrPropertyWithValue("title", "newTitle");
        thenNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionWhenUpdatingMissingEan() throws Exception {
        // Given
        when(ficheRepository.findOneByEanAndOwner(anyString(), anyString())).thenReturn(Optional.empty());


        //When (+ expect exception)
        Assertions.assertThrows(EanNotFoundException.class, () -> {
            ficheStoreService.save(new FicheDvd("123"), "username", true);
        });

        //Then
        verify(ficheRepository, times(1)).findOneByEanAndOwner("123", "username");
        thenNoMoreInteractions();
    }

    private FicheEntity newFicheEntity(String ean) {
        FicheEntity entity = new FicheEntity();
        entity.setEan(ean);
        entity.setUsername("username");
        return entity;
    }

    private FicheEntity newFicheEntity(String ean, String title) {
        FicheEntity entity = new FicheEntity();
        entity.setEan(ean);
        entity.setUsername("username");
        entity.setTitle(title);
        return entity;
    }

    private void thenNoMoreInteractions() {
        verifyNoMoreInteractions(ficheRepository);
    }
}
