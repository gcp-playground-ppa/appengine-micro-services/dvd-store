package fr.ppaquin.microservices.fichestore;

import com.github.ahunigel.test.security.oauth2.MockTokenServices;
import fr.ppaquin.microservices.dvdstore.utils.AbstractEndPoint;
import fr.ppaquin.microservices.dvdstore.utils.WithOAuthSubject;
import fr.ppaquin.microservices.fichestore.dto.FicheDvd;
import fr.ppaquin.microservices.fichestore.exception.EanAlreadyDefinedException;
import fr.ppaquin.microservices.fichestore.exception.EanNotFoundException;
import fr.ppaquin.microservices.fichestore.service.FicheStoreService;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.UnsupportedEncodingException;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@WebAppConfiguration
@SpringBootTest(classes = FichestoreServiceApplication.class)
@MockTokenServices
@AutoConfigureMockMvc
public class FicheDvdEndPointSpec extends AbstractEndPoint {

    @MockBean
    FicheStoreService ficheStoreService;

    FicheDvd dummyFiche1 = new FicheDvd("123");

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void createApiShouldCreateDvdAndReturnFiche() throws Exception {
        givenFicheServiceReturning(newFicheDvd(1, "123"));

        whenRequest(put("/fiche").content(toJson(dummyFiche1)).contentType(APPLICATION_JSON_VALUE));

        thenShouldCreateFicheAndReturnCreated(newFicheDvd(1, "123"));
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void createApiShouldReturn409WhenFicheAlreadyExist() throws Exception {
        givenFicheServiceThrowing(new EanAlreadyDefinedException("EAN 123 already exist"));

        whenRequest(put("/fiche").content(toJson(dummyFiche1)).contentType(APPLICATION_JSON_VALUE));

        thenShouldCreateFicheAndReturnConflict();
    }

    @Test
    void createApiShouldReturn401WhenPutAsAnonymous() throws Exception {
        whenRequest(put("/fiche").content(toJson(dummyFiche1)).contentType(APPLICATION_JSON_VALUE));

        thenShouldReturnUnauthorized();
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void updateApiShouldUpdateFicheAndReturnUpdatedFiche() throws Exception {
        givenFicheServiceReturning(newFicheDvd(1, "123"));

        whenRequest(post("/fiche").content(toJson(dummyFiche1)).contentType(APPLICATION_JSON_VALUE));

        thenShouldUpdateFicheAndReturnUpdated(newFicheDvd(1, "123"));
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void updateApiShouldReturn204WhenUpdatingEanThatDoesntExist() throws Exception {
        givenFicheServiceThrowing(new EanNotFoundException("EAN 123 does not exist exist"));

        whenRequest(post("/fiche").content(toJson(dummyFiche1)).contentType(APPLICATION_JSON_VALUE));

        thenShouldUpdateFicheAndReturnNoTontent();
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void getFicheShouldSearchUsersFicheForEan() throws Exception {
        givenFicheServiceReturning(newFicheDvd(1, "123"));

        whenRequest(get("/fiche/123"));

        thenShouldSearchFicheAndReturn(newFicheDvd(1, "123"));
    }

    @Test
    @WithOAuthSubject(scopes = {"${dvdstore.authority.dvd}"})
    void getFicheShouldReturn204WhenUpdatingEanThatDoesntExist() throws Exception {
        givenFicheServiceThrowing(new EanNotFoundException("EAN 123 does not exist exist"));

        whenRequest(get("/fiche/123"));

        thenShouldSearchFicheAndReturnNoContent();
    }

    private void givenFicheServiceThrowing(Exception e) throws Exception {
        when(ficheStoreService.save(any(FicheDvd.class), anyString(), anyBoolean())).thenThrow(e);
        if (!(e instanceof EanAlreadyDefinedException)) {
            when(ficheStoreService.findUsersFiche(anyString(), anyString())).thenThrow(e);
        }
    }

    private void givenFicheServiceReturning(FicheDvd ficheDvd) throws Exception {
        when(ficheStoreService.save(any(FicheDvd.class), anyString(), anyBoolean())).thenReturn(ficheDvd);
        when(ficheStoreService.findUsersFiche(anyString(), anyString())).thenReturn(ficheDvd);
    }

    private void thenShouldCreateFicheAndReturnCreated(FicheDvd expected) throws Exception {
        // Services save called with update to false
        verify(ficheStoreService, times(1)).save(new FicheDvd("123"), "username", false);
        // Return expected object
        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(expected).isEqualTo(mapper.readValue(receivedJson, FicheDvd.class));
        // Status 201
        assertEquals(CREATED.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldCreateFicheAndReturnConflict() throws EanAlreadyDefinedException, EanNotFoundException {
        // Service save called with update to false
        verify(ficheStoreService, times(1)).save(new FicheDvd("123"), "username", false);
        // Status 409 Conflict
        assertEquals(CONFLICT.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldUpdateFicheAndReturnUpdated(FicheDvd expected) throws Exception {
        // Services save called with update to true
        verify(ficheStoreService, times(1)).save(new FicheDvd("123"), "username", true);
        // Return expected object
        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(expected).isEqualTo(mapper.readValue(receivedJson, FicheDvd.class));
        // Status 201
        assertEquals(OK.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldUpdateFicheAndReturnNoTontent() throws EanAlreadyDefinedException, EanNotFoundException {
        // Services save called with update to true
        verify(ficheStoreService, times(1)).save(new FicheDvd("123"), "username", true);
        // Status 204 No Content
        assertEquals(NO_CONTENT.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldSearchFicheAndReturn(FicheDvd expected) throws EanNotFoundException, UnsupportedEncodingException, com.fasterxml.jackson.core.JsonProcessingException {
        // Services findUsersFiche called with user and ean
        verify(ficheStoreService, times(1)).findUsersFiche("123", "username");
        // Return expected object
        String receivedJson = mvcResult.getResponse().getContentAsString();
        assertThat(expected).isEqualTo(mapper.readValue(receivedJson, FicheDvd.class));
        // Status 200 OK
        assertEquals(OK.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldSearchFicheAndReturnNoContent() throws EanAlreadyDefinedException, EanNotFoundException {
        // Services findUsersFiche called with user and ean
        verify(ficheStoreService, times(1)).findUsersFiche("123", "username");
        // Status 204 No Content
        assertEquals(NO_CONTENT.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private void thenShouldReturnUnauthorized() {
        // Status 401 Unauthorized
        assertEquals(UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
        // No unexpected call
        thenNoMoreInteractions();
    }

    private FicheDvd newFicheDvd(int dvdfr, String ean) {
        return new FicheDvd(1, "123", "", 0, "", "", "", emptyList());
    }

    private void thenNoMoreInteractions() {
        verifyNoMoreInteractions(ficheStoreService);
    }
}
