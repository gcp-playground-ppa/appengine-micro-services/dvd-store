package fr.ppaquin.microservices.storage.config;

import org.springframework.cloud.gcp.data.firestore.repository.config.EnableReactiveFirestoreRepositories;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableReactiveFirestoreRepositories({"fr.ppaquin.microservices.storage"})
public class FirestoreConfig {
}
