package fr.ppaquin.microservices.storage.entity;

import com.google.cloud.firestore.annotation.DocumentId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.cloud.gcp.data.firestore.Document;

@Data
@Document(collectionName = "dvds")
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class DvdEntity {

    @DocumentId
    private String documentId;

    @NonNull
    private String ean;

    @NonNull
    private String title;

    @NonNull
    private String cover;

    @NonNull
    private String username;

    private Integer dvdfr;

    private Integer year;

    public DvdEntity(Long id, String ean, String title, String cover, String username, Integer dvdfr, Integer year) {
        this.documentId = id ==null ? "0": id.toString();
        this.ean = ean;
        this.title = title;
        this.cover = cover;
        this.username = username;
        this.dvdfr= dvdfr;
        this.year = year;
    }

    public void setId(Long id) {
        this.documentId = id ==null ? "0" : id.toString();
    }

    public Long getId() {
        return Long.parseLong(documentId);
    }
}
