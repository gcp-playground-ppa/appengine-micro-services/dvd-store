package fr.ppaquin.microservices.storage.entity;

import com.google.cloud.firestore.annotation.DocumentId;
import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@Data
public class FicheEntity {

    @DocumentId
    private String documentId;
    private int tmdbId;

    @NonNull
    private String ean = "";

    @NonNull
    private String title = "";
    private int year;
    private String cover = "";
    private String backdrop = "";
    private String overview = "";

    private List<String> genres = new ArrayList<>();

    @NonNull
    private String username = "";
}
