package fr.ppaquin.microservices.storage.dao;

import fr.ppaquin.microservices.storage.entity.FicheEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
public class FicheRepository {

    @Autowired
    private FicheFirestore ficheFirestore;

    public Optional<FicheEntity> findOneByEanAndOwner(String ean, String userName) {
        return Optional.ofNullable(ficheFirestore.findByUsername(userName).filter(d -> d.getEan().equalsIgnoreCase(ean)).blockFirst());
    }

    public FicheEntity save(FicheEntity entity) {
        if (!StringUtils.hasText(entity.getDocumentId())) {
            entity.setDocumentId(Long.toString(System.currentTimeMillis()));
        }
        return ficheFirestore.save(entity).block();
    }
}
