package fr.ppaquin.microservices.storage.dao;

import fr.ppaquin.microservices.storage.entity.DvdEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DvdRepository {

    @Autowired
    private DvdFirestore dvdFirestore;

    public Optional<DvdEntity> findByEanAndOwner(String ean, String userName) {
        return Optional.ofNullable(dvdFirestore.findByUsername(userName).filter(d -> d.getEan().equalsIgnoreCase(ean)).blockFirst());
    }

    public DvdEntity save(DvdEntity entity) {
        if (!StringUtils.hasText(entity.getDocumentId())) {
            entity.setDocumentId(Long.toString(System.currentTimeMillis()));
        }
        return dvdFirestore.save(entity).block();
    }

    public List<DvdEntity> findByOwner(String userName) {
        return dvdFirestore.findByUsername(userName).collect(Collectors.toList()).block();
    }

    public void delete(DvdEntity dvdEntity) {
        dvdFirestore.delete(dvdEntity).block();
    }
}
