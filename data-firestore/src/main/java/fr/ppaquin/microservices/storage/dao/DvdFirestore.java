package fr.ppaquin.microservices.storage.dao;

import fr.ppaquin.microservices.storage.entity.DvdEntity;
import reactor.core.publisher.Flux;

import org.springframework.cloud.gcp.data.firestore.FirestoreReactiveRepository;

    public interface DvdFirestore extends FirestoreReactiveRepository<DvdEntity> {

    Flux<DvdEntity> findByUsername(String username);
}
