package fr.ppaquin.microservices.storage.dao;

import fr.ppaquin.microservices.storage.entity.FicheEntity;
import reactor.core.publisher.Flux;

import org.springframework.cloud.gcp.data.firestore.FirestoreReactiveRepository;

public interface FicheFirestore extends FirestoreReactiveRepository<FicheEntity> {

    Flux<FicheEntity> findByUsername(String username);
}
