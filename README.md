# DVD Store service

Microservices project for DVDs endpoints.

Micro services : 
* API Default : simple endpoint for dvds
* API fiche : Descriptive endpoint for dvds
* (to come) API Gateway : Zuul gateway for APIs

Libraries : 
* data-firestore : use this library to use GCP Firestore service
* data-jpa : use this library to use standard JPA

# Use Locally
1. Compile `mvn clean package`
2. Start server `java -jar target\dvdstore-service-0.0.1-SNAPSHOT.jar`
3. This microservice is deployed with default profile "dev" to localhost port 9003, it's an authenticated API using OAuth2, Bearer token is mandatory.
4. Swagger access (local only) : http://localhost:9003/swagger-ui.html
   * user: john
   * password : 123
   * client : newClient
   * secret : newClientSecret

# Deploy to Google Cloud App Engine

1.  Compile : `mvn clean package -Pgcp`
2.  Connect to your GCP account, verify that API Cloud Compile is enabled.
4.  Deploy to GCP : `mvn appengine:deploy`

# Eléments de context

* Ce service est actuellement utilisé directement par l'application reactjs. Une API Gateway (Zuul) pourra être utilisé comme point d'entrée à tous les services. 
* Eureka et Ribbon ne sont pas utiles sur une infrastructure Google Cloud car la création d'instances en cas de forte demande est automatique, et le load-balancing est géré par le DNS Google.
* Dans le cadre de la découverte de Google Cloud AppEngine Always Free, la persistence sera migré prochainement vers Cloud Firestore qui offre un quota gratuit. L'utilisation d'une petite base de données MySQL n'est mise en place que pour l'exemple d'utilisation JPA (et QueryDSL) sur Spring Boot et sera retiré prochainement.

# Todo : 
* Mise en place Zuul comme API Gateway
* Migration de la persistance vers GCP Firestore
